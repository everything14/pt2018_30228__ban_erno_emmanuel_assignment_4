package Model;

public class SpendingAccount extends Account{
    public void depositMoney(int sum) {
        this.sum.set(this.sum.intValue() + sum);
    }
    public int withdrawMoney(int sum) {
        if (this.sum.intValue() >= sum) {
            this.sum.set(this.sum.intValue() - sum);
            return sum;
        }
        return 0;
    }

    @Override
    public String getType() {
        return "Spending";
    }
}
