package View;

import Controller.Bank;
import Controller.BankProc;
import Model.Account;
import Model.Person;
import Model.SavingAccount;
import Model.SpendingAccount;

import javax.swing.*;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Map;
import java.util.Set;

public class AccountView {
    private Bank bank;
    private JFrame jFrame = new JFrame("View All Accounts");
    private JPanel jPanel = new JPanel();
    private DefaultTableModel model;
    private JTable jTable;
    private JButton addNew = new JButton("Add New Entry");
    private JButton delete = new JButton("Delete Entry");
    private JButton view = new JButton("View Existing Accounts");
    private JButton addWithdraw = new JButton("Add / Withdraw");

    public AccountView() {
        jPanel.setLayout(new BoxLayout(jPanel, BoxLayout.Y_AXIS));
        model = new DefaultTableModel(0, 4);
        jTable = new JTable(model);
        addHead();
        addEmptyRow();
        jTable.setRowSelectionInterval(0, 0);
        setTableListener();
        setDeleteButtonListener();
        setButtonListeners();
        JPanel jPanel2 = new JPanel();
        jPanel2.setLayout(new BoxLayout(jPanel2, BoxLayout.X_AXIS));
        jPanel2.add(addNew);
        jPanel2.add(delete);
        jPanel2.add(view);
        jPanel2.add(addWithdraw);
        jPanel.add(new JScrollPane(jTable));
        jPanel.add(jPanel2);
        jFrame.add(jPanel);
        jFrame.setVisible(true);
        jFrame.pack();
    }
    public void setBank(Bank bank) {
        this.bank = bank;
    }
    private void addHead() {
        jTable.getTableHeader().getColumnModel().getColumn(0).setHeaderValue("Client CNP (String(13))");
        jTable.getTableHeader().getColumnModel().getColumn(1).setHeaderValue("Account ID (int) - unique");
        jTable.getTableHeader().getColumnModel().getColumn(2).setHeaderValue("Available sum (int)");
        jTable.getTableHeader().getColumnModel().getColumn(3).setHeaderValue("Account Type (Saving/Spending)");
    }
    private void addEmptyRow() {
        Object[] row = {"", "", "", ""};
        model.addRow(row);
    }
    private void saveAccount(int editingRow) {
        Account account;
        if (model.getValueAt(editingRow, 0).toString().length() == 13 &&
                model.getValueAt(editingRow, 3).toString().equals("Saving")) {
            account = new SavingAccount();
        } else
        if (model.getValueAt(editingRow, 0).toString().length() == 13 &&
                model.getValueAt(editingRow, 3).toString().equals("Spending")) {
            account = new SpendingAccount();
        }
        else return;
        account.depositMoney(Integer.valueOf(model.getValueAt(editingRow, 2).toString()));
        account.setID(Integer.valueOf(model.getValueAt(editingRow, 1).toString()));
        Person person = new Person(model.getValueAt(editingRow, 0).toString());
        if (!bank.exists(account))
            bank.addAccount(person, account);
    }
    private void updateView() {
        for (Person person: BankProc.persons) {
            Set<Account> accounts = BankProc.holderAccounts.get(person);
            if (accounts != null) {
                for (Account account : accounts) {
                    Object[] row = new Object[jTable.getColumnCount()];
                    row[0] = person.getCNP();
                    row[1] = account.getID();
                    row[2] = account.getSum();
                    row[3] = account.getType();
                    model.addRow(row);
                }
            }
        }
    }
    private void setTableListener() {
        model.addTableModelListener(new TableModelListener() {
            @Override
            public void tableChanged(TableModelEvent e) {
                if (jTable.getEditingRow() >= 0) {
                    boolean isRowFull = true;
                    for (int i = 0; i < jTable.getColumnCount(); i++)
                        if (model.getValueAt(jTable.getEditingRow(), i) == "")
                            isRowFull = false;
                    if (isRowFull)
                        saveAccount(jTable.getEditingRow());
                }
            }
        });
    }
    private void setDeleteButtonListener() {
        delete.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (jTable.getSelectedRow() >= 0) {
                    if (model.getValueAt(jTable.getSelectedRow(), 0) != null && model.getValueAt(jTable.getSelectedRow(), 1) != null && model.getValueAt(jTable.getSelectedRow(), 3) != null) {
                        Account account;
                        if (model.getValueAt(jTable.getSelectedRow(), 3).toString().equals("Saving")) {
                            account = new SavingAccount();
                        } else if (model.getValueAt(jTable.getSelectedRow(), 3).toString().equals("Spending")) {
                            account = new SpendingAccount();
                        } else return;
                        account.setID(Integer.valueOf(model.getValueAt(jTable.getSelectedRow(), 1).toString()));
                        Person person = new Person(model.getValueAt(jTable.getSelectedRow(), 0).toString());
                        if (bank.exists(account))
                            bank.removeAccount(person, account);
                    }
                    model.removeRow(jTable.getSelectedRow());
                }
                if (jTable.getModel().getRowCount() != 0)
                    jTable.setRowSelectionInterval(0, 0);
            }
        });
    }
    private void setButtonListeners() {
        addNew.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) { addEmptyRow(); }
        });
        view.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (jTable.getEditingRow() < 0) {
                    int rowCount = model.getRowCount();
                    for (int i = rowCount - 1; i >= 0; i--)
                        model.removeRow(i);
                    updateView();
                }

            }
        });
        addWithdraw.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (jTable.getEditingRow() < 0) {
                    AddWithdraw addWithdraw = new AddWithdraw();
                    addWithdraw.setBank(bank);
                }
            }
        });
    }
}
