package Controller;

import Model.Account;
import Model.Person;

import java.util.*;

public interface BankProc {
    Map<Person, Set<Account>> holderAccounts = Collections.synchronizedMap(new HashMap<>());
    Set<Person> persons = Collections.synchronizedSet(new HashSet<>());

    public void addPerson(Person person);
    public void removePerson(Person person);
    public void addAccount(Person person, Account account);
    public void removeAccount(Person person, Account account);
    public boolean exists(Person person);
    public boolean exists(Account account);
    public Person findPerson(String cnp);
    public Account findAccount(int id);
    public boolean rightHolder(Person person, Account account);
}
