package Model;

import java.util.concurrent.atomic.AtomicInteger;

public abstract class Account {
    int ID;
    AtomicInteger sum = new AtomicInteger(0);

    public abstract void depositMoney(int sum);
    public abstract int withdrawMoney(int sum);
    public abstract String getType();
    public void setID(int ID) {
        this.ID = ID;
    }
    public void editAccount(int ID, int sum) {
        ID = ID;
        this.sum.set(sum);
    }
    public int getID() {
        return ID;
    }
    public int getSum() {
        return sum.intValue();
    }
    public void setSum(int sum) {
        this.sum.set(sum);
    }
}
