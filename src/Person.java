package Model;

import java.util.Observable;
import java.util.Observer;

public class Person implements Observer{
    private String cnp;

    public Person(String cnp) {
        assert (cnp.length() == 13);
        this.cnp = cnp;
    }
    @Override
    public int hashCode() {
        assert (cnp.length() == 13);
        return Integer.valueOf(cnp.substring(9, 12));
    }
    @Override
    public boolean equals(Object object) {
        if (! (object instanceof Person)) {
            throw new IllegalArgumentException();
        }
        return (this.hashCode() == object.hashCode());
    }
    @Override
    public void update(Observable o, Object arg) {

    }
    public void editPerson(String cnp) {
        this.cnp = cnp;
    }
    public String getCNP() {
        return cnp;
    }
}
