package View;

import Controller.Bank;
import Controller.BankProc;
import Model.Account;
import Model.Person;

import javax.swing.*;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Map;

public class PersonView {
    private Bank bank;
    private JFrame jFrame = new JFrame("View All Persons");
    private JPanel jPanel = new JPanel();
    private DefaultTableModel model;
    private JTable jTable;
    private JButton addNew = new JButton("Add New Entry");
    private JButton delete = new JButton("Delete Entry");
    private JButton view = new JButton("View Existing Persons");
    private JButton addWithdraw = new JButton("Add / Withdraw");

    public PersonView() {
        jPanel.setLayout(new BoxLayout(jPanel, BoxLayout.Y_AXIS));
        model = new DefaultTableModel(0, 1);
        jTable = new JTable(model);
        addHead();
        addEmptyRow();
        jTable.setRowSelectionInterval(0, 0);
        setTableListener();
        setDeleteButtonListener();
        setButtonListeners();
        JPanel jPanel2 = new JPanel();
        jPanel2.setLayout(new BoxLayout(jPanel2, BoxLayout.X_AXIS));
        jPanel2.add(addNew);
        jPanel2.add(delete);
        jPanel2.add(view);
        jPanel2.add(addWithdraw);
        jPanel.add(new JScrollPane(jTable));
        jPanel.add(jPanel2);
        jFrame.add(jPanel);
        jFrame.setVisible(true);
        jFrame.pack();
    }
    private void addHead() {
        jTable.getTableHeader().getColumnModel().getColumn(0).setHeaderValue("Client CNP (String(13))");
    }
    private void addEmptyRow() {
        Object[] row = {""};
        model.addRow(row);
    }
    public void setBank(Bank bank) {
        this.bank = bank;
    }
    private void savePerson(int editingRow) {
        if (model.getValueAt(jTable.getSelectedRow(), 0).toString().length() == 13) {
            Person person = new Person(model.getValueAt(editingRow, 0).toString());
            if (!bank.exists(person))
                bank.addPerson(person);
//        else {
//
//        }
        }
    }
    private void updateView() {
        for (Person person: BankProc.persons) {
            Object[] row = new Object[jTable.getColumnCount()];
            row[0] = person.getCNP();
            model.addRow(row);
        }
    }
    private void setTableListener() {
        model.addTableModelListener(new TableModelListener() {
            @Override
            public void tableChanged(TableModelEvent e) {
                if (jTable.getEditingRow() >= 0) {
                    boolean isRowFull = true;
                    for (int i = 0; i < jTable.getColumnCount(); i++)
                        if (model.getValueAt(jTable.getEditingRow(), i) == "")
                            isRowFull = false;
                    if (isRowFull)
                        savePerson(jTable.getEditingRow());
                }
            }
        });
    }
    private void setDeleteButtonListener() {
        delete.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (jTable.getSelectedRow() >= 0 && model.getValueAt(jTable.getSelectedRow(), 0) != null) {
                    if (model.getValueAt(jTable.getSelectedRow(), 0).toString().length() == 13) {
                        Person person = new Person(model.getValueAt(jTable.getSelectedRow(), 0).toString());
                        if (bank.exists(person))
                            bank.removePerson(person);
                    }
                        model.removeRow(jTable.getSelectedRow());
                    }
                    if (jTable.getModel().getRowCount() != 0)
                        jTable.setRowSelectionInterval(0, 0);
            }
        });
    }
    private void setButtonListeners() {
        addNew.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) { addEmptyRow(); }
        });
        view.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (jTable.getEditingRow() < 0) {
                    int rowCount = model.getRowCount();
                    for (int i = rowCount - 1; i >= 0; i--)
                        model.removeRow(i);
                    updateView();
                    bank.saveData();
                }
            }
        });
        addWithdraw.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (jTable.getEditingRow() < 0) {
                    AddWithdraw addWithdraw = new AddWithdraw();
                    addWithdraw.setBank(bank);
                }
            }
        });
    }
}
