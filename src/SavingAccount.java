package Model;

public class SavingAccount extends Account{
    private int interest = 5;

    public void depositMoney(int sum) {
        super.sum.set(sum);
    }
    public void setInterest(int interest) {
        this.interest = interest;
    }
    public int withdrawMoney(int value) {
        int sum = super.sum.intValue();
        this.sum.set(0);
        return sum + sum*interest;
    }

    @Override
    public String getType() {
        return "Saving";
    }
}
