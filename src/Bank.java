package Controller;

import Model.Account;
import Model.Person;
import View.AccountView;
import View.PersonView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class Bank implements BankProc {
    @Override
    public void addPerson(Person person) {
        BankProc.persons.add(person);
    }

    @Override
    public void removePerson(Person person) {
        BankProc.persons.remove(person);
    }

    @Override
    public void addAccount(Person person, Account account) {
        Set<Account> accounts = BankProc.holderAccounts.get(person);
        if (accounts == null) {
            accounts = Collections.synchronizedSet(new HashSet<>());
        }
        accounts.add(account);
        if (BankProc.persons.contains(person))
            BankProc.holderAccounts.put(person, accounts);
    }

    @Override
    public void removeAccount(Person person, Account account) {
        Set<Account> accounts = BankProc.holderAccounts.get(person);
        if (accounts != null) {
            for (Account account1 : accounts)
                if (account1.getID() == account.getID()) {
                    BankProc.holderAccounts.get(person).remove(account1);
                    return;
                }
        }
    }

    @Override
    public boolean exists(Person person) {
        for (Person person1: persons)
            if (person1.equals(person))
                return true;
        return false;
    }

    @Override
    public boolean exists( Account account) {
        for (Person person: BankProc.persons) {
            Set<Account> accounts = BankProc.holderAccounts.get(person);
            if (accounts != null)
                for (Account account1 : accounts)
                    if (account1.getID() == account.getID())
                        return true;
        }
        return false;
    }

    @Override
    public Person findPerson(String cnp) {
        for (Person person : persons)
            if (person.getCNP().equals(cnp))
                return person;
        return null;
    }

    @Override
    public Account findAccount(int id) {
        for (Person person : BankProc.persons) {
            Set<Account> accounts = BankProc.holderAccounts.get(person);
            if (accounts != null)
            for (Account account1: accounts)
                if (account1.getID() == id)
                    return account1;
        }
        return null;
    }

    @Override
    public boolean rightHolder(Person person, Account account) {
        Set<Account> accounts = BankProc.holderAccounts.get(person);
        if (accounts != null)
        for (Account account1: accounts)
            if (account1.getID() == account.getID())
                return true;
        return false;
    }

    public Account getAccount(Account account) {
        for (Person person : persons) {
            Set<Account> accounts = BankProc.holderAccounts.get(person);
            if (accounts != null)
            for (Account account1 : accounts)
                if (account1.getID() == account.getID())
                    return account1;
        }
        return null;
    }

    public static void main(String[] args) {
        Bank bank = new Bank();
        AccountView accountView = new AccountView();
        accountView.setBank(bank);
        PersonView personView = new PersonView();
        personView.setBank(bank);
    }

    public void saveData() {
        PrintWriter writer;
        try {
            writer = new PrintWriter("C:\\Users\\EMMANUEL\\Desktop\\TemaTPpt\\Persons.txt", "UTF-8");
            for (Person person: persons) {
                writer.println(person.getCNP());
            }
            writer.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }
    public void loadData() {
        File file = new File("./Persons.txt");
    }
}
