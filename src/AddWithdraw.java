package View;

import Controller.Bank;
import Model.Account;
import Model.Person;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AddWithdraw {
    private Bank bank;
    private String cnp;
    private int idCont;
    private int sum;
    private String action;
    private JFrame jFrame = new JFrame("Add / Withdraw");
    private JPanel jPanel = new JPanel();
    private JTextField jTextField1 = new JTextField("CNP");
    private JTextField jTextField2 = new JTextField("ID Cont");
    private JTextField jTextField3 = new JTextField("Sum (just for Spending Account)");
    private JTextField jTextField4 = new JTextField("Add / Withdraw");

    public AddWithdraw() {
        jPanel.setLayout(new BoxLayout(jPanel, BoxLayout.Y_AXIS));
        jPanel.add(jTextField1);
        jPanel.add(jTextField2);
        jPanel.add(jTextField3);
        jPanel.add(jTextField4);
        setTextFieldListeners();
        setNumericTextFieldListeners();
        jFrame.add(jPanel);
        jFrame.setVisible(true);
        jFrame.pack();
    }
    public void setBank(Bank bank) {
        this.bank = bank;
    }
    public void addWithdraw() {
        if (action != null && cnp != null) {
            Person person = bank.findPerson(cnp);
            Account account = bank.findAccount(idCont);
            if (action.equals("Add")) {
                if (account != null && account.getType().equals("Spending")) {
                    System.out.println(sum + " has been added to account " + account.getID());
                    account.setSum(account.getSum() + sum);
                }
            } else
            if (action.equals("Withdraw")) {
                if (account != null && person != null) {
                    if (account.getSum() >= sum && bank.rightHolder(person, account))
                        if (account.getType().equals("Spending"))
                            System.out.println(sum + " has been withdrawn from account " + account.getID());
                        else
                            System.out.println(account.getSum() + " has been withdrawn from account " + account.getID());
                        account.withdrawMoney(sum);
                }
            }
        }
    }
    private void setTextFieldListeners() {
        jTextField1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cnp = jTextField1.getText();
                addWithdraw();
            }
        });
        jTextField4.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                action = jTextField4.getText();
                addWithdraw();
            }
        });
    }
    private void setNumericTextFieldListeners() {
        jTextField2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    idCont = Integer.valueOf(jTextField2.getText());
                    addWithdraw();
                }
                catch (Exception e1) {
                    System.out.println("Not a number");
                }
            }
        });
        jTextField3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    sum = Integer.valueOf(jTextField3.getText());
                    addWithdraw();
                }
                catch (Exception e1) {
                    System.out.println("Not a number");
                }
            }
        });
    }
}
