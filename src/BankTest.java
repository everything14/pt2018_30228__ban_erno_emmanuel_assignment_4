package JUnit;

import Controller.Bank;
import Model.Account;
import Model.Person;
import Model.SavingAccount;
import Model.SpendingAccount;
import org.junit.jupiter.api.Test;

public class BankTest {
    @Test
    public void addPerson() {
        Bank bank = new Bank();
        Person person1 = new Person("1234567890123");
        bank.addPerson(person1);
        assert (bank.exists(person1));
    }
    @Test
    public void removePerson() {
        Bank bank = new Bank();
        Person person1 = new Person("1234567890123");
        bank.addPerson(person1);
        bank.removePerson(person1);
        assert (!bank.exists(person1));
    }
    @Test
    public void addAccounts() {
        Bank bank = new Bank();
        Person person1 = new Person("1234567890123");
        Person person2 = new Person("2345678901234");
        bank.addPerson(person1);
        bank.addPerson(person2);
        Account account1 = new SpendingAccount();
        Account account2 = new SavingAccount();
        bank.addAccount(person1, account1);
        assert (bank.exists(account1));
        bank.addAccount(person2, account2);
        assert (bank.exists(account2));
    }
    @Test
    public void removeAccounts() {
        Bank bank = new Bank();
        Person person1 = new Person("1234567890123");
        Person person2 = new Person("2345678901234");
        bank.addPerson(person1);
        bank.addPerson(person2);
        Account account1 = new SpendingAccount();
        account1.setID(1);
        Account account2 = new SavingAccount();
        account2.setID(2);
        bank.addAccount(person1, account1);
        assert (bank.exists(account1));
        bank.addAccount(person2, account2);
        bank.removeAccount(person1, account1);
        assert (bank.exists(account2));
        assert (!bank.exists(account1));
    }
}
